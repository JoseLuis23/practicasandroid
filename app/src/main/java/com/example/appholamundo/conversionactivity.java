package com.example.appholamundo;

import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class conversionactivity extends AppCompatActivity {

    private EditText txtGrados;
    private RadioButton rdbCel, rdbFa;
    private TextView resultado;

    private Button btnCalcular, btnLimpiar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.conversionactivity);
        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtGrados.getText().toString().matches("")) {
                    Toast.makeText(conversionactivity.this, "Capture el dato:", Toast.LENGTH_LONG).show();
                } else {
                    float grados = Float.parseFloat(txtGrados.getText().toString());
                    if (rdbCel.isChecked()) {
                        grados = (grados * 9 / 5) + 32;
                        resultado.setText(String.format("%.2f °F", grados));
                    } else if (rdbFa.isChecked()) {
                        grados = (grados - 32) * 5 / 9;
                        resultado.setText(String.format("%.2f °C", grados));
                    } else {
                        Toast.makeText(conversionactivity.this, "Seleccione una opción de conversión", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtGrados.setText("");
                rdbCel.setChecked(false);
                rdbFa.setChecked(false);
                resultado.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void iniciarComponentes() {
        txtGrados = (EditText) findViewById(R.id.txtCantidad);
        rdbCel = (RadioButton) findViewById(R.id.rdbCel);
        rdbFa = (RadioButton) findViewById(R.id.rdbFa);
        resultado = (TextView) findViewById(R.id.txtResultado);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
    }
}
