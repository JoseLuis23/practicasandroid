package com.example.appholamundo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class IMCactivity extends AppCompatActivity {

    private TextView lblResultado;
    private EditText txtAltura, txtPeso;
    private Button btnCalcular, btnCerrar, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_imcactivity);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        //Inicialización de los componentes
        initComponents();
        //Configuracion del botón Calcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validación de campos vacíos
                if(txtPeso.getText().toString().matches("") || txtAltura.getText().toString().matches("")){
                    //Impresión del aviso de error
                    Toast.makeText(IMCactivity.this, "Información faltante",Toast.LENGTH_SHORT).show();
                } else{
                    //Captura de los valores ingresados
                    float peso = Float.parseFloat(txtPeso.getText().toString());
                    float altura = Float.parseFloat(txtAltura.getText().toString()) / 100;
                    //Validación de valores correctos para el cálculo
                    if (peso < 0 || altura < 0) {
                        Toast.makeText(IMCactivity.this, "Información incorrecta", Toast.LENGTH_SHORT).show();
                    } else {
                        //Cálculo del IMC
                        float imc = peso / (altura * altura);
                        //Impresión del resultado
                        lblResultado.setText(String.format("Tu IMC ES: %.2f", imc));
                    }
                }
            }
        });
        //Configuración del botón Limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtPeso.setText("");
                lblResultado.setText("TU IMC ES:");
            }
        });
        //Configuración del botón cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }

    private void initComponents(){
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
    }
}